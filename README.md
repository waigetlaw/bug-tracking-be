# bug-tracking-be

Backend for the prototype Bug Tracking Application

requires a folder `sslcert` with a `server.cert` and `server.key` file for https. Can use openssl to generate these files.

requires .env file at root:

```
# Set to production when deploying to production
NODE_ENV=development
LOG_LEVEL=info

# Node.js server configuration
SERVER_PORT=8080

DB_CONNECTION_STRING=mongodb://localhost:27017
DB_USERNAME=username
DB_PASSWORD=password
DB_NAME=bugTrackingDB

# Application JWT configuration
JWT_ISSUER=bug-tracking-app-backend
JWT_AUDIENCE=bug-tracking-app-frontend
JWT_SECRET=some-secret
```