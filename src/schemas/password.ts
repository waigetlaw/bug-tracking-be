import { ParamSchema } from "express-validator";

export const passwordSchema: Record<string, ParamSchema> = {
    password: {
        in: ["body"],
        exists: {
            errorMessage: "password is required"
        },
        isString: true,
        isLength: {
            errorMessage: "password should be between 8-100 characters long",
            options: { max: 100, min: 8 }
        },
        matches: {
            options: /^.*[^a-zA-Z0-9\s].*$/, // checks at least 1 char that isn't a-z, A-Z, 0-9 or whitespace (assumes it will be a special char)
            errorMessage: "password must contain at least 1 special character"
        }
    }
};
