import { ParamSchema } from "express-validator";

export const passcodeSchema: Record<string, ParamSchema> = {
    passcode: {
        in: ["body"],
        isNumeric: true,
        isLength: {
            errorMessage: "must be length 6",
            options: { max: 6, min: 6 }
        }
    }
};
