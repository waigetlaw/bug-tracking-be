import { ParamSchema } from "express-validator";

export const credentialSchema: Record<string, ParamSchema> = {
    username: {
        in: ["body"],
        exists: {
            errorMessage: "username is required"
        },
        isString: true,
        isLength: {
            errorMessage: "max length 30",
            options: { max: 30 }
        },
        notEmpty: true,
        trim: true,
        matches: {
            options: /^[A-Za-z0-9_@.-]*$/,
            errorMessage: "Only characters a-z, A-Z, 0-9 and the following four special characters are allowed: [. - _ @]. No spaces are allowed."
        }
    },
    password: {
        in: ["body"],
        exists: {
            errorMessage: "password is required"
        },
        isString: true,
        isLength: {
            errorMessage: "max length 100",
            options: { max: 100 }
        }
    }
};
