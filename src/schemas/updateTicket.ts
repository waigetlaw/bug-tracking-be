import { ParamSchema } from "express-validator";
import { EPriority, EStatus, EType } from "../interfaces/ITicket";

export const updateTicketSchema: Record<string, ParamSchema> = {
    type: {
        in: ["body"],
        isString: true,
        isIn: {
            options: [Object.keys(EType)],
            errorMessage: `type must be one of the following values: ${Object.keys(EType)}`
        },
        optional: true
    },
    title: {
        in: ["body"],
        exists: {
            errorMessage: "title is required"
        },
        isString: true,
        optional: true,
        isLength: {
            errorMessage: "max length 80",
            options: { max: 80 }
        }
    },
    description: {
        in: ["body"],
        exists: {
            errorMessage: "description is required"
        },
        isString: true,
        isLength: {
            errorMessage: "max length 500",
            options: { max: 500 }
        },
        optional: true
    },
    assigned: {
        in: ["body"],
        isString: true,
        optional: true,
        isLength: {
            errorMessage: "max length 30",
            options: { max: 30 }
        }
    },
    status: {
        in: ["body"],
        isString: true,
        isIn: {
            options: [Object.keys(EStatus)],
            errorMessage: "status must be one of the following values: ['OPEN', 'RESOLVED', 'CLOSED']"
        },
        optional: true
    },
    priority: {
        in: ["body"],
        isString: true,
        isIn: {
            options: [Object.keys(EPriority)],
            errorMessage: "priority must be one of the following values: ['LOW', 'MEDIUM', 'HIGH']"
        },
        optional: true
    }
};
