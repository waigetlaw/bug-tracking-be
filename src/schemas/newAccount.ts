import { ParamSchema } from "express-validator";

export const newAccountSchema: Record<string, ParamSchema> = {
    username: {
        in: ["body"],
        exists: {
            errorMessage: "username is required"
        },
        isString: true,
        isLength: {
            errorMessage: "max length 30",
            options: { max: 30 }
        },
        notEmpty: true,
        trim: true,
        matches: {
            options: /^[A-Za-z0-9_@.-]*$/,
            errorMessage: "Only characters a-z, A-Z, 0-9 and the following four special characters are allowed: [. - _ @]. No spaces are allowed."
        }
    },
    password: {
        in: ["body"],
        exists: {
            errorMessage: "password is required"
        },
        isString: true,
        isLength: {
            errorMessage: "password should be between 8-100 characters long",
            options: { max: 100, min: 8 }
        },
        matches: {
            options: /^.*[^a-zA-Z0-9\s].*$/, // checks at least 1 char that isn't a-z, A-Z, 0-9 or whitespace (assumes it will be a special char)
            errorMessage: "password must contain at least 1 special character"
        }
    },
    recaptcha: {
        in: ["body"],
        exists: {
            errorMessage: "recaptcha is required"
        },
        isString: true
    }
};
