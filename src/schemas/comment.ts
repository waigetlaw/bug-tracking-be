import { ParamSchema } from "express-validator";

export const commentSchema: Record<string, ParamSchema> = {
    ticketId: {
        in: ["body"],
        exists: {
            errorMessage: "ticket id or number is required"
        }
    },
    comment: {
        in: ["body"],
        exists: {
            errorMessage: "comment is required"
        },
        isString: true,
        isLength: {
            errorMessage: "max length 500 characters",
            options: { max: 500 }
        }
    }
};
