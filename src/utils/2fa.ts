import speakeasy from "speakeasy";

export function generateNewSecret() {
    return speakeasy.generateSecret({ name: "BugTrackingApp" });
}

export function verifyPasscode(secret: string, token: string) {
    return speakeasy.totp.verify({
        secret,
        encoding: "base32",
        token
    });
}