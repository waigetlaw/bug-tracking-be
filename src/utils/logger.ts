import pino from "pino";
import dotenv from "dotenv";

dotenv.config();

export const logger = pino({
    level: process.env.LOG_LEVEL || "info",
    prettyPrint: true,
    redact: {
        paths: ['res.headers["set-cookie"]', 'req.headers.cookie', 'body.password', 'body.passcode'],
        censor: "***REDACTED***"
    }
});

export default logger;