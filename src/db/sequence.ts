import { SequenceModel } from "../models/sequence";

export async function getNextValue() {
    const sequence = await SequenceModel.findOneAndUpdate(
        { _id: "ticketNumber" },
        { $inc: { value: 1 } },
        { new: true }
    );

    if (!sequence) {
        await SequenceModel.create({ _id: "ticketNumber", value: 1 });
        return 1;
    }

    return sequence && sequence.value;
}