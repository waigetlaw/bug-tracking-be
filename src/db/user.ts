import { IUser } from "../interfaces/IUser";
import { UserModel } from "../models/user";

export const addUser = async (user: IUser) => {
    return UserModel.create(user);
}

export const findUser = async (username: string) => {
    return UserModel.findOne({ username }).exec();
}

export const getUsers = async () => {
    return UserModel.find({}, 'username').exec();
}

export const updateUserByUsername = async ({ username, password, secret2FA, enabled2FA }: IUser) => {
    return UserModel.findOneAndUpdate({ username }, {
        ...(password && { password }),
        ...(secret2FA !== undefined && { secret2FA }),
        ...(enabled2FA != null && { enabled2FA }),
    }, { new: true, projection: '-_id -password -secret2FA' });
}