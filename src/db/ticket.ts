import { EStatus, IComment, ITicket } from "../interfaces/ITicket";
import { TicketModel } from "../models/ticket";

export const createTicket = async (ticket: ITicket) => {
    const newTicket = await TicketModel.create(ticket);
    const cloneTicket = newTicket.toObject();
    delete cloneTicket.__v;
    return cloneTicket;
}

export const getTickets = async () => {
    return TicketModel.find({}).exec();
}

export const getTicketById = async (id: string) => {
    return TicketModel.findById(id);
}

export const getTicketByNumber = async (no: number) => {
    return TicketModel.findOne({ no });
}

export const updateTicketById = async (id: string, { type, title, description, timestamp, assigned, status, priority }: ITicket) => {
    return TicketModel.findByIdAndUpdate(id, {
        ...(type && { type }),
        ...(title !== undefined && { title }),
        ...(description !== undefined && { description }),
        ...(timestamp && { timestamp }),
        ...(assigned !== undefined && { assigned }),
        ...(status && { status }),
        ...(priority && { priority })
    }, { new: true })
}

export const addCommentToTicketById = async (id: string, comment: IComment) => {
    return TicketModel.findByIdAndUpdate(id, {
        $push: {
            comments: comment
        }
    }, { new: true })
}

export const reopenTicketById = async (id: string) => {
    return TicketModel.findByIdAndUpdate(id, {
        status: EStatus.OPEN
    }, { new: true })
}