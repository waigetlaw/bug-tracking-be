import { Document } from "mongoose";

export interface ISequence {
    value: number;
}

export interface ISequenceDoc extends Document, ISequence { };