import { Document } from "mongoose";

export interface IUser {
    username: string;
    password?: string;
    secret2FA?: string;
    enabled2FA?: boolean;
}

export interface IUserDoc extends Document, IUser { }