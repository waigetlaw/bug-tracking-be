export interface ICredentials {
    username: string;
    password: string;
    passcode?: string;
}
