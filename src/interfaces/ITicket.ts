import { Document } from "mongoose";

export interface ITicket {
    type: EType;
    createdBy?: string;
    title: string;
    no?: number;
    description: string;
    timestamp: string;
    assigned: string;
    status: EStatus;
    priority: EPriority;
    comments: IComment[];
}

export interface IComment {
    timestamp: string;
    username: string;
    comment: string;
}

export enum EStatus { OPEN = "OPEN", RESOLVED = "RESOLVED", CLOSED = "CLOSED" }

export enum EPriority { LOW = "LOW", MEDIUM = "MEDIUM", HIGH = "HIGH" }

export enum EType { DEVELOPMENT = "DEVELOPMENT", TESTING = "TESTING", PRODUCTION = "PRODUCTION" }

export interface ITicketDoc extends Document, ITicket { };