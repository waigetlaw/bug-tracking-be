export interface ILoginResponse {
    jwt?: string;
    username: string;
    enabled2FA: boolean;
}
