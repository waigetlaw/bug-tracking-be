export interface IJWT {
    username: string;
    _id: string;
}