import express, { CookieOptions, Request, Response } from "express";
import PinoHttp from "express-pino-logger";
import mongoose from "mongoose";
import dotenv from "dotenv";
import { checkSchema } from "express-validator";
import { credentialSchema } from "./schemas/credentials";
import logger from "./utils/logger";
import validator from "./middleware/validator";
import auth from "./middleware/auth";
import cookieParser from "cookie-parser";
import logRequest from "./middleware/logRequest";
import { createUser, login, getUsers, generateAndSetSecret, confirm2fa, disable2fa, login2fa, updateUserPassword, getUserByUsername } from "./domain/user";
import { addComment, createTicket, getTicketByNumberOrId, getTickets, reopenTicket, updateTicketByNumberOrId } from "./domain/ticket";
import { IJWT } from "./interfaces/IJWT";
import { passcodeSchema } from "./schemas/passcode";
import { credential2faSchema } from "./schemas/credentials2fa";
import https from "https";
import fs from "fs";
import cors, { CorsOptions } from "cors";
import reCAPTCHA from "recaptcha2";
import { newAccountSchema } from "./schemas/newAccount";
import { passwordSchema } from "./schemas/password";
import { updateTicketSchema } from "./schemas/updateTicket";
import getUsernameFromAuthCookie from "./middleware/getUsernameFromAuthCookie";
import { commentSchema } from "./schemas/comment";
import { newTicketSchema } from "./schemas/newTicket";

dotenv.config();

const privateKey = fs.readFileSync('sslcert/server.key', 'utf8');
const certificate = fs.readFileSync('sslcert/server.cert', 'utf8');

const siteKey = process.env.RECAPTCHA_SITE_KEY || '';
const secretKey = process.env.RECAPTCHA_SECRET_KEY || '';

const recaptcha = new reCAPTCHA({ siteKey, secretKey });

const expressLogger = PinoHttp({ logger });

const port = process.env.SERVER_PORT || 8443;
const app = express();
const TOKEN_COOKIE_NAME = "token";
const cookieOptions: CookieOptions = { maxAge: 60 * 60 * 1000, httpOnly: true, path: "/" };

app.use(express.json());
app.use(expressLogger);
app.use(cookieParser());
app.use(getUsernameFromAuthCookie);
app.use(logRequest);

const corsOptions: CorsOptions = {
    origin: ['http://localhost:3000'],
    credentials: true
}

app.use(cors(corsOptions));

/**
 * Public APIs
 */
app.post("/login", checkSchema(credentialSchema), validator, async (req: Request, res: Response) => {
    try {
        const response = await login(req.body);
        if (!response) return res.sendStatus(401);

        const { jwt, ...loginResponse } = response;

        if (jwt) {
            res.cookie(TOKEN_COOKIE_NAME, jwt, cookieOptions);
        }
        return res.status(200).json(loginResponse);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
});

app.post("/login2fa", checkSchema(credential2faSchema), validator, async (req: Request, res: Response) => {
    try {
        const response = await login2fa(req.body);
        if (!response) return res.sendStatus(401);

        const { jwt, ...loginResponse } = response;
        res.cookie(TOKEN_COOKIE_NAME, jwt, cookieOptions);
        return res.status(200).json(loginResponse);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
});

app.post("/logout", async (req: Request, res: Response) => {
    res.clearCookie(TOKEN_COOKIE_NAME);
    return res.sendStatus(204);
});

app.post("/users", checkSchema(newAccountSchema), validator, async (req: Request, res: Response) => {
    // check ReCaptcha
    try {
        await recaptcha.validate(req.body.recaptcha);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(400);
    }

    try {
        await createUser(req.body);
        return res.sendStatus(201);
    } catch (e) {
        if ((e as Error).message.includes("E11000")) {
            return res.sendStatus(409);
        }
        logger.error(e);
        return res.sendStatus(500);
    }
})

/**
 * Private APIs
 */
app.put("/users", auth, checkSchema(passwordSchema), validator, async (req: Request, res: Response) => {
    try {
        const token = (req as any).decodedJwt as IJWT;
        await updateUserPassword({ ...req.body, username: token.username });
        return res.sendStatus(204);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
})

app.get("/users", auth, async (req: Request, res: Response) => {
    try {
        const users = await getUsers();
        return res.status(200).json(users);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
})

app.post("/tickets", auth, checkSchema(newTicketSchema), validator, async (req: Request, res: Response) => {
    try {
        if (req.body.assigned) {
            const assignedUser = await getUserByUsername(req.body.assigned);
            if (!assignedUser) {
                res.status(400).json({ "error": `Assigned user '${req.body.assigned}' does not exist` });
            }
        }
        const token = (req as any).decodedJwt as IJWT;
        const ticket = await createTicket({ ...req.body, createdBy: token.username });
        return res.status(201).json(ticket);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
})

app.post("/reopen/:ticketId", auth, async (req: Request, res: Response) => {
    try {
        const token = (req as any).decodedJwt as IJWT;
        const ticket = await reopenTicket(req.params.ticketId);
        return res.status(200).json(ticket);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
});

app.get("/tickets", auth, async (req: Request, res: Response) => {
    try {
        const tickets = await getTickets();
        return res.status(200).json(tickets);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
})

app.get("/tickets/:ticketId", auth, async (req: Request, res: Response) => {
    try {
        const ticket = await getTicketByNumberOrId(req.params.ticketId);
        return res.status(200).json(ticket);
    } catch (e) {
        logger.error(e);
        res.sendStatus(500);
    }
});

app.put("/tickets/:ticketId", auth, checkSchema(updateTicketSchema), validator, async (req: Request, res: Response) => {
    try {
        if (req.body.assigned) {
            const assignedUser = await getUserByUsername(req.body.assigned);
            if (!assignedUser) {
                res.status(400).json({ "error": `Assigned user '${req.body.assigned}' does not exist` });
            }
        }
        const token = (req as any).decodedJwt as IJWT;
        const username = token.username;
        const updatedTicket = await updateTicketByNumberOrId(username, req.params.ticketId, req.body);
        if (!updatedTicket) return res.sendStatus(400);
        return res.status(200).json(updatedTicket);
    } catch (e) {
        logger.error(e);
        res.sendStatus(500);
    }
});

app.post("/comments", auth, checkSchema(commentSchema), validator, async (req: Request, res: Response) => {
    try {
        const token = (req as any).decodedJwt as IJWT;
        const username = token.username;
        const updatedTicket = await addComment(username, req.body.ticketId, req.body.comment);
        if (!updatedTicket) return res.sendStatus(400);
        return res.status(200).json(updatedTicket);
    } catch (e) {
        logger.error(e);
        res.sendStatus(500);
    }
})

app.get("/get2fa", auth, async (req: Request, res: Response) => {
    try {
        const token = (req as any).decodedJwt as IJWT;
        const secret = await generateAndSetSecret(token.username);
        return res.status(200).json({ secret });
    } catch (e) {
        logger.error(e);
        res.sendStatus(500);
    }
});

app.post("/confirm2fa", auth, checkSchema(passcodeSchema), validator, async (req: Request, res: Response) => {
    try {
        const token = (req as any).decodedJwt as IJWT;
        if (await confirm2fa(token.username, req.body.passcode)) {
            return res.sendStatus(204);
        }
        return res.sendStatus(400);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
});

app.post("/disable2fa", auth, checkSchema(passcodeSchema), validator, async (req: Request, res: Response) => {
    try {
        const token = (req as any).decodedJwt as IJWT;
        if (await disable2fa(token.username, req.body.passcode)) {
            return res.sendStatus(204);
        }
        return res.sendStatus(400);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(500);
    }
})

const httpsServer = https.createServer({ key: privateKey, cert: certificate }, app);

httpsServer.listen(port, async () => {
    const options: mongoose.ConnectionOptions = {
        user: process.env.DB_USERNAME,
        pass: process.env.DB_PASSWORD,
        dbName: process.env.DB_NAME,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    };
    const connectionString = process.env.DB_CONNECTION_STRING || '';

    await mongoose.connect(connectionString, options)
    logger.info(`server started at https://localhost:${port}`);
})
