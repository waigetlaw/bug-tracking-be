import mongoose from "mongoose";
import { IUser, IUserDoc } from "../interfaces/IUser";

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const UserSchema = new Schema<IUser>({
    _id: { type: ObjectId, auto: true },
    username: { type: String, required: true, index: true, unique: true, lowercase: true, trim: true, dropDups: true },
    password: { type: String, required: true },
    secret2FA: { type: String, default: "" },
    enabled2FA: { type: Boolean, default: false },
    __v: { type: Number, select: false }
}, { collation: { locale: "en", strength: 2 } });

export const UserModel = mongoose.model<IUserDoc>("users", UserSchema);
