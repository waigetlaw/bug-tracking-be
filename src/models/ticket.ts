import mongoose from "mongoose";
import { EPriority, EStatus, EType, IComment, ITicket, ITicketDoc } from "../interfaces/ITicket";

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const CommentSchema = new Schema<IComment>({
    timestamp: { type: Date, required: true },
    username: { type: String, required: true },
    comment: { type: String, required: true }
});

const TicketSchema = new Schema<ITicket>({
    _id: { type: ObjectId, auto: true },
    type: { type: String, enum: Object.values(EType), required: true },
    createdBy: { type: String, required: true },
    title: { type: String, required: true },
    no: { type: Number, required: true },
    description: { type: String, default: "" },
    timestamp: { type: Date, required: true },
    assigned: { type: String, default: "" },
    status: { type: String, enum: Object.values(EStatus), required: true },
    priority: { type: String, enum: Object.values(EPriority), required: true },
    comments: { type: [CommentSchema], required: true, default: [] },
    __v: { type: Number, select: false }
});

export const TicketModel = mongoose.model<ITicketDoc>("tickets", TicketSchema);
