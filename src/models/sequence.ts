import mongoose from "mongoose";
import { ISequence, ISequenceDoc } from "../interfaces/ISequence";

const Schema = mongoose.Schema;

const SequenceSchema = new Schema<ISequence>({
    _id: { type: String },
    value: { type: Number },
    __v: { type: Number, select: false }
});

export const SequenceModel = mongoose.model<ISequenceDoc>("sequence", SequenceSchema);
