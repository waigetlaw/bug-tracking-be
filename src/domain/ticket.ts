import { getNextValue } from "../db/sequence";
import { EStatus, IComment, ITicket } from "../interfaces/ITicket";
import * as dbTicket from "../db/ticket";
import logger from "../utils/logger";
import ObjectID from "mongoose";

export async function createTicket(ticket: ITicket) {
    const ticketNumber = await getNextValue();
    return dbTicket.createTicket({ ...ticket, no: ticketNumber, status: EStatus.OPEN, timestamp: new Date().toISOString() });
}

export async function getTickets() {
    return dbTicket.getTickets();
}

export async function getTicketByNumberOrId(numberOrId: string) {
    let ticket;
    const validId = ObjectID.isValidObjectId(numberOrId);
    if (validId) {
        ticket = await dbTicket.getTicketById(numberOrId);
    }
    if (!ticket) {
        try {
            ticket = await dbTicket.getTicketByNumber(parseInt(numberOrId, 10));
        } catch (e) {
            logger.debug(e);
        }
    }
    return ticket;
}

export async function updateTicketByNumberOrId(username: string, numberOrId: string, ticket: ITicket) {
    const currentTicket = await getTicketByNumberOrId(numberOrId);
    if (!currentTicket) return null;

    // only creator and assigned user can edit the ticket
    if (username !== currentTicket.createdBy && username !== currentTicket.assigned) return null;

    // cannot edit a closed ticket
    if (currentTicket.status === EStatus.CLOSED) return null;

    // if status update, check the new status is valid based on current value
    if (ticket.status) {
        const allowedStatus = getAllowedStatusUpdates(currentTicket.status);
        if (!allowedStatus.includes(ticket.status)) return null;
    }

    // update ticket
    const ticketWithTimestamp = { ...ticket, timestamp: new Date().toISOString() };
    return dbTicket.updateTicketById(currentTicket._id, ticketWithTimestamp);
}

export async function addComment(username: string, ticketId: string, comment: string) {
    const currentTicket = await getTicketByNumberOrId(ticketId);
    if (!currentTicket || currentTicket.status === EStatus.CLOSED) return null;

    const timestamp = new Date().toISOString();
    const newComment: IComment = { username, comment, timestamp };

    return dbTicket.addCommentToTicketById(currentTicket._id, newComment);
}

export async function reopenTicket(ticketId: string) {
    const currentTicket = await getTicketByNumberOrId(ticketId);
    if (!currentTicket || currentTicket.status !== EStatus.CLOSED) return null; // should be closed

    return dbTicket.reopenTicketById(currentTicket._id);
}

function getAllowedStatusUpdates(currentStatus: EStatus) {
    switch (currentStatus) {
        case EStatus.OPEN: {
            return [EStatus.OPEN, EStatus.RESOLVED];
        }
        case EStatus.RESOLVED: {
            return [EStatus.RESOLVED, EStatus.CLOSED];
        }
        case EStatus.CLOSED: {
            return [EStatus.CLOSED]
        }
    }
}
