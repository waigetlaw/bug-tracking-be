import { ICredentials } from "../interfaces/ICredentials";
import bcrypt from "bcrypt";
import { IUser } from "../interfaces/IUser";
import * as dbUser from "../db/user";
import { generateJWT } from "../utils/jwt";
import { ILoginResponse } from "../interfaces/ILoginResponse";
import * as twoFactorAuth from "../utils/2fa";

export async function createUser(credentials: ICredentials) {
    const hash = await bcrypt.hash(credentials.password, 10);

    const user: IUser = {
        username: credentials.username,
        password: hash
    };

    return dbUser.addUser(user);
};

export async function getUserByUsername(username: string) {
    return dbUser.findUser(username);
}

export async function getUsers() {
    const users = await dbUser.getUsers();
    return users.map((user) => user.username);
}

export async function login(credentials: ICredentials): Promise<ILoginResponse | null> {
    const user = await getUserByUsername(credentials.username);
    if (user && await bcrypt.compare(credentials.password, user.password || '')) {
        const response = { username: credentials.username, enabled2FA: !!user.enabled2FA };
        if (!user.enabled2FA) {
            const jwt = generateJWT(credentials.username);
            return { ...response, jwt };
        }
        return response;
    }
    return null;
}

export async function login2fa(credentials: ICredentials): Promise<ILoginResponse | null> {
    const user = await getUserByUsername(credentials.username);
    if (!user) return null; // user not found
    if (!(user.enabled2FA && user.secret2FA)) return null; // this api is for 2fa login only

    // check password
    const validPassword = await bcrypt.compare(credentials.password, user.password || '');
    const validPasscode = twoFactorAuth.verifyPasscode(user.secret2FA, credentials.passcode || '');

    // if wrong password or passcode, deny entry
    if (!(validPassword && validPasscode)) return null;

    const jwt = generateJWT(credentials.username);
    return { username: credentials.username, enabled2FA: user.enabled2FA, jwt };
}

export async function updateUserPassword(credentials: ICredentials) {
    const hash = await bcrypt.hash(credentials.password, 10);
    const user: IUser = {
        username: credentials.username,
        password: hash
    };

    return dbUser.updateUserByUsername(user);
}

export async function generateAndSetSecret(username: string) {
    const secret = twoFactorAuth.generateNewSecret();
    const updateUser: IUser = { username, secret2FA: secret.base32, enabled2FA: false }
    await dbUser.updateUserByUsername(updateUser);
    return secret;
}

export async function confirm2fa(username: string, passcode: string) {
    const user = await getUserByUsername(username);
    if (!(user && user.secret2FA)) return false;

    if (twoFactorAuth.verifyPasscode(user.secret2FA, passcode)) {
        await dbUser.updateUserByUsername({ username, enabled2FA: true });
        return true;
    }
    return false;
}

export async function disable2fa(username: string, passcode: string) {
    const user = await getUserByUsername(username);
    if (!(user && user.secret2FA)) return false;

    if (twoFactorAuth.verifyPasscode(user.secret2FA, passcode)) {
        await dbUser.updateUserByUsername({ username, enabled2FA: false, secret2FA: "" });
        return true;
    }
    return false;
}