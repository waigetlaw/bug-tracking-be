import { Request, Response, NextFunction } from "express";
import logger from "../utils/logger";

export function logRequest(req: Request, res: Response, next: NextFunction) {
    req.log.info(`[${req.method}: ${req.url}] request received ${(req as any).username ? `from user [${(req as any).username}]` : ''}`);
    logger.debug({ body: req.body }, "request body")
    next();
}

export default logRequest;
