import { Request, Response, NextFunction } from "express";
import { VerifyOptions } from "jsonwebtoken";
import { logger } from "../utils/logger";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import { IJWT } from "../interfaces/IJWT";

dotenv.config();

export function auth(req: Request, res: Response, next: NextFunction) {
    if (!req.cookies.token) {
        logger.error("No authentication token found");
        return res.sendStatus(401);
    }
    const options: VerifyOptions = {
        issuer: process.env.JWT_ISSUER,
        audience: process.env.JWT_AUDIENCE
    };

    try {
        const decoded = jwt.verify(req.cookies.token, process.env.JWT_SECRET || '', options) as IJWT;
        (req as any).decodedJwt = decoded;
        next();
    } catch (e) {
        return res.sendStatus(401);
    }
}

export default auth;
