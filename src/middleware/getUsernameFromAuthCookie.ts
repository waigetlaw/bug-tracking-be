import e, { Request, Response, NextFunction } from "express";
import { VerifyOptions } from "jsonwebtoken";
import { logger } from "../utils/logger";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import { IJWT } from "../interfaces/IJWT";

dotenv.config();

export function getUsernameFromAuthCookie(req: Request, res: Response, next: NextFunction) {
    if (req.cookies.token) {
        const decoded = jwt.decode(req.cookies.token) as IJWT;
        (req as any).username = decoded.username;
    }
    next();
}

export default getUsernameFromAuthCookie;
