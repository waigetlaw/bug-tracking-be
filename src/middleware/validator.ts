import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import logger from "../utils/logger";

export function validator(req: Request, res: Response, next: NextFunction) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        logger.debug(errors.array(), "Validation errors:");
        return res.status(400).json({ errors: errors.array() });
    }
    next();
}

export default validator;
